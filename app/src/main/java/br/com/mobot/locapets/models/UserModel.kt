package br.com.mobot.locapets.models

import java.io.Serializable

data class UserModel(val name: String, val email: String, val tokenGoogle: String) : Serializable