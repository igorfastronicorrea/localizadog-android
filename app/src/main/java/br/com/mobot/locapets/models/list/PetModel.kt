package br.com.mobot.locapets.models.list

import java.io.Serializable

data class PetModel(
    val name: String,
    val photo: List<String>,
    val _id: String,
    val description: String,
    val latitude: String,
    val longitude: String,
    val city: String,
    val km: Int,
    val whatsapp: String,
    val user: String,
    val createAt: String,
    val lostAt: String
) : Serializable