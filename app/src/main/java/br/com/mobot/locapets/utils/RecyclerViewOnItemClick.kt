package br.com.mobot.locapets.utils

interface RecyclerViewOnItemClick {
    fun onItemClick(id: String, lostAt: String)
}