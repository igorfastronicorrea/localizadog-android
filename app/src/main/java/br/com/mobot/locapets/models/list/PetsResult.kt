package br.com.mobot.locapets.models.list

import java.io.Serializable

data class PetsResult(var petsResult: List<PetModel>) : Serializable