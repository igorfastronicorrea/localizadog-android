package br.com.mobot.locapets.models

import java.io.Serializable

data class UserResponseModel(val user: UserResponse) : Serializable