package br.com.mobot.locapets.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class APIClient{

    companion object {
        fun getDataApi(url: String) : Retrofit {

            return Retrofit.Builder()
                .baseUrl(url)
                .client(OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        }
    }

}