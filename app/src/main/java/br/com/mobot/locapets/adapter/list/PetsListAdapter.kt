package br.com.mobot.locapets

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.mobot.locapets.models.list.PetModel
import br.com.mobot.locapets.models.list.PetsResult
import br.com.mobot.locapets.utils.RecyclerViewOnItemClick
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_pet.view.*


class PetsListAdapter(
    var dataPets: ArrayList<PetModel>?,
    var context: Context,
    val recyclerviewListener: RecyclerViewOnItemClick
) : RecyclerView.Adapter<PetsListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.item_pet, p0, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        if (dataPets != null) {
            return dataPets!!.size
        } else {
            return 0
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, i: Int) {
        val dataPet = dataPets?.get(i)

        holder.let {
            it.bindView(dataPet)
        }

        holder.itemView.setOnClickListener { recyclerviewListener.onItemClick(dataPet!!._id, dataPet?.lostAt) }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(dataPet: PetModel?) {

            val imgPhoto = itemView.imgPhoto
            val txtTimeLost = itemView.txtTimeLost
            val txtName = itemView.txtName
            val txtLocationLost = itemView.txtLocationLost

            Glide.with(context).load(dataPet?.photo?.get(0)).placeholder(R.drawable.loading_spinner).into(imgPhoto)
            txtTimeLost.text = dataPet?.lostAt
            txtName.text = dataPet?.name
            txtLocationLost.text = "Última vez visto há ${dataPet?.km} km de você"

        }
    }
}