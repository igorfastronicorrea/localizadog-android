package br.com.mobot.flatsbr

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.mobot.locapets.R
import com.bumptech.glide.Glide
import com.stfalcon.imageviewer.StfalconImageViewer
import kotlinx.android.synthetic.main.item_photo.view.*


class DetailAdapter(
    var dataPhotos: List<String>,
    var context: Context
) : RecyclerView.Adapter<DetailAdapter.ViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.item_photo, p0, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataPhotos.size
    }

    override fun onBindViewHolder(holder: ViewHolder, i: Int) {
        val dataCombos = dataPhotos[i]

        holder.let {
            it.bindView(dataCombos, i, dataPhotos)
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(photo: String, position: Int, dataPhotos: List<String>) {
            val imgPhoto = itemView.imgPhoto

            Glide.with(context).load(photo).into(imgPhoto)

            imgPhoto.setOnClickListener {
                StfalconImageViewer.Builder<String>(context, dataPhotos) { view, image ->
                    Glide.with(context).load(image).into(view)
                }
                    .withStartPosition(position)
                    .show(true)
            }
        }
    }
}