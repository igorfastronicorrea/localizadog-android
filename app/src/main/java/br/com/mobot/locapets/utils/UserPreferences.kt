package br.com.mobot.locapets.utils

import android.content.Context
import kotlin.reflect.KProperty

class UserPreferences(
    context: Context
) {

    val PREFERENCENAME = "SharedPreferenceUser"
    val PREFERENCE_NAME_KEY = "SharedPreferenceUserName"
    val PREFERENCE_EMAIL_KEY = "SharedPreferenceUserEmail"
    val PREFERENCE_TOKEN_KEY = "SharedPreferenceUserTokenGoogle"
    val PREFERENCE_USERID_KEY = "SharedPreferenceUserId"

    val preference = context.getSharedPreferences(PREFERENCENAME, Context.MODE_PRIVATE)


    fun getUserName(): String {
        return preference.getString(PREFERENCE_NAME_KEY, "")
    }

    fun getUserId(): String {
        return preference.getString(PREFERENCE_USERID_KEY, "")
    }


    fun getUserEmail(): String {
        return preference.getString(PREFERENCE_EMAIL_KEY, "")
    }

    fun getUserTokenGoogle(): String {
        return preference.getString(PREFERENCE_TOKEN_KEY, "")
    }

    fun setUserName(
        name: String
    ) {
        val editor = preference.edit()
        editor
            .putString(PREFERENCE_NAME_KEY, name)
        editor.apply()
    }

    fun setUserEmail(
        email: String
    ) {
        val editor = preference.edit()
        editor
            .putString(PREFERENCE_EMAIL_KEY, email)
        editor.apply()
    }

    fun setUserTokenGoogle(
        tokenGoogle: String
    ) {
        val editor = preference.edit()
        editor
            .putString(PREFERENCE_TOKEN_KEY, tokenGoogle)
        editor.apply()
    }

    fun setUserId(
        tokenGoogle: String
    ) {
        val editor = preference.edit()
        editor
            .putString(PREFERENCE_USERID_KEY, tokenGoogle)
        editor.apply()
    }
}