package br.com.mobot.locapets.network

import android.net.Uri
import br.com.mobot.locapets.models.*
import br.com.mobot.locapets.models.list.PetsResult
import br.com.mobot.locapets.utils.Constants
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface APIInterface {

    @Multipart
    @POST("photo/upload")
    fun uploadImage(@Part image: MultipartBody.Part): Call<DataImage>

    @POST("userid/{userId}/pet/create")
    fun createPost(@Path("userId") userId: String, @Body body: PetCreateModel): Call<MessageModel>

    @POST("auth/register")
    fun createUser(@Body body: UserModel): Call<UserResponseModel>

    @GET("pets")
    fun getList(@Query("latitude") latitude: String, @Query("longitude") longitude: String, @Query("page") page: String) : Call<PetsResult>

    @GET("pets/{petId}")
    fun getDetail(@Path("petId") petId: String) : Call<DetailPetsModel>

}