package br.com.mobot.locapets.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DataImage(val url: String) : Serializable