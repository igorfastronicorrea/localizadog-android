package br.com.mobot.locapets.adapter.register

import br.com.mobot.locapets.R

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.marginRight
import androidx.recyclerview.widget.RecyclerView
import br.com.mobot.locapets.utils.RecyclerViewOnItemClick
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_pet.view.*


class ImageAdapter(
    var dataPhotos: ArrayList<String>,
    var context: Context
) : RecyclerView.Adapter<ImageAdapter.ViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.item_photo_upload, p0, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataPhotos.size
    }

    override fun onBindViewHolder(holder: ViewHolder, i: Int) {
        val dataCombos = dataPhotos[i]

        holder.let {
            it.bindView(dataCombos, i, dataPhotos)
        }

    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(photo: String, position: Int, dataPhotos: List<String>) {
            val imgPhoto = itemView.imgPhoto
            Glide.with(context).load(photo).into(imgPhoto)
        }
    }
}