package br.com.mobot.locapets.models

import java.io.Serializable

data class PetCreateModel(
    var name: String,
    var description: String,
    var photo: List<String>,
    var latitude: String,
    var longitude: String,
    var city: String,
    var km: String,
    var whatsapp: String
) : Serializable