package br.com.mobot.locapets.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import br.com.mobot.locapets.R
import br.com.mobot.locapets.models.MessageModel
import br.com.mobot.locapets.models.UserModel
import br.com.mobot.locapets.models.UserResponseModel
import br.com.mobot.locapets.network.APIClient
import br.com.mobot.locapets.network.APIInterface
import br.com.mobot.locapets.ui.createPost.RegisterActivity
import br.com.mobot.locapets.utils.Constants
import br.com.mobot.locapets.utils.UserPreferences
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.act_register.*
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : BaseActivity() {
    var auth: FirebaseAuth? = null
    private lateinit var googleSignInClient: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)

        signInButton.setOnClickListener { startSignIn() }
    }


    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            Toast.makeText(this, "User ID: $user.uid", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, "Erro, tente novamente mais tarde", Toast.LENGTH_LONG).show()
        }
    }

    private fun startSignIn() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)

    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account!!)
            } catch (e: ApiException) {
                Log.w(TAG, "Google sign in failed", e)
                updateUI(null)
            }
        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.id!!)
        showProgressDialog()

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        auth?.signInWithCredential(credential)
            ?.addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    val user = auth!!.currentUser
                    createAccountAPI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    updateUI(null)
                }

            }
    }

    private fun createAccountAPI(user: FirebaseUser?) {
        val name = user!!.displayName
        val email = user!!.email
        val tokenGoogle = auth!!.currentUser?.zzg()

        val bodyUser = UserModel(name!!, email!!, tokenGoogle!!)
        val retrofit =
            APIClient.getDataApi(Constants.BASE_URL_UPLOADIMAGE).create(APIInterface::class.java)
        val call = retrofit.createUser(bodyUser)

        call.enqueue(object : Callback<UserResponseModel> {
            override fun onFailure(call: Call<UserResponseModel>, t: Throwable) {
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<UserResponseModel>,
                response: Response<UserResponseModel>
            ) {
                if (response.body() != null) {
                    hideProgressDialog()
                    var bodyUser = response.body()
                    saveSharedPreference(bodyUser!!)
                }
            }

        })

    }

    private fun saveSharedPreference(userResponseModel: UserResponseModel) {
        val userPreference = UserPreferences(this)
        userPreference.setUserName(userResponseModel.user.name)
        userPreference.setUserEmail(userResponseModel.user.email)
        userPreference.setUserTokenGoogle(userResponseModel.user.tokenGoogle)
        userPreference.setUserId(userResponseModel.user._id)
        val i = Intent(this, RegisterActivity::class.java)
        startActivity(i)
        finish()
    }

    companion object {
        private const val TAG = "GoogleActivity"
        private const val RC_SIGN_IN = 9001
    }

}
