package br.com.mobot.locapets.ui.list

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.mobot.locapets.PetsListAdapter
import br.com.mobot.locapets.R
import br.com.mobot.locapets.models.MessageModel
import br.com.mobot.locapets.models.list.PetModel
import br.com.mobot.locapets.models.list.PetsResult
import br.com.mobot.locapets.network.APIClient
import br.com.mobot.locapets.network.APIInterface
import br.com.mobot.locapets.ui.LoginActivity
import br.com.mobot.locapets.ui.createPost.RegisterActivity
import br.com.mobot.locapets.utils.RecyclerViewOnItemClick
import br.com.mobot.locapets.ui.detail.DetailActivity
import br.com.mobot.locapets.utils.Constants
import br.com.mobot.locapets.utils.UserPreferences
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.act_register.*
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    var dataPets: ArrayList<PetModel>? = null
    var adapter: PetsListAdapter? = null
    var isLoading = false
    var page = 1
    var haveAPI = true
    var latitude = ""
    var longitude = ""

    private var mFusedLocationClient: FusedLocationProviderClient? = null
    protected var mLastLocation: Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dataPets = ArrayList<PetModel>()
        adapter = PetsListAdapter(dataPets, this, recyclerItemClickListener)
        rvPets.adapter = adapter
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvPets.layoutManager = layoutManager

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        rvPets.setOnClickListener {
            val i = Intent(this, DetailActivity::class.java)
            startActivity(i)
        }

        fabRegisterPet.setOnClickListener {
            val userPreference = UserPreferences(this)
            val email = userPreference.getUserEmail()
            if (email.isEmpty() && email.equals("")) {
                val i = Intent(this, LoginActivity::class.java)
                startActivity(i)
            } else {
                val i = Intent(this, RegisterActivity::class.java)
                startActivity(i)
            }
        }

        rvPets.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if (dy > 0) {
                    if (!isLoading && (layoutManager.findLastCompletelyVisibleItemPosition() == dataPets!!.size - 1)) {

                        progressBar.visibility = View.VISIBLE
                        if (haveAPI) {
                            page = page + 1
                        }
                        getDataAPI(page)
                        Log.i("PASSOU", page.toString())
                    }
                }

            }
        })


        if (checkPermissions()) {
            btnPermiteLocalization.visibility = View.GONE
            rvPets.visibility = View.VISIBLE
            progressBar.visibility = View.VISIBLE
        } else {
            rvPets.visibility = View.GONE
            btnPermiteLocalization.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }


        btnPermiteLocalization.setOnClickListener { requestPermissions() }

    }

    private val recyclerItemClickListener = object :
        RecyclerViewOnItemClick {
        override fun onItemClick(id: String, lostAt: String) {
            val intent = Intent(this@MainActivity, DetailActivity::class.java)
            intent.putExtra("idPet", id)
            intent.putExtra("lostAt", lostAt)
            startActivity(intent)
        }
    }

    private fun getDataAPI(page: Int) {
        isLoading = true
        val retrofit =
            APIClient.getDataApi(Constants.BASE_URL_UPLOADIMAGE).create(APIInterface::class.java)
        val call = retrofit.getList(latitude, longitude, page.toString())

        call.enqueue(object : Callback<PetsResult> {
            override fun onFailure(call: Call<PetsResult>, t: Throwable) {
                Log.i("LIST", "error")
                isLoading = false
                progressBar.visibility = View.GONE
            }

            override fun onResponse(call: Call<PetsResult>, response: Response<PetsResult>) {
                if (response.body() != null) {

                    var pets = response.body()
                    if (pets!!.petsResult.size > 0) {
                        for (i in 0 until pets!!.petsResult.size) {
                            dataPets!!.add(pets.petsResult[i])
                        }
                        adapter!!.notifyDataSetChanged()
                        haveAPI = true
                    } else {
                        haveAPI = false
                    }
                    isLoading = false
                    progressBar.visibility = View.GONE
                }
            }

        })
    }

    public override fun onStart() {
        super.onStart()

        if (!checkPermissions()) {
            requestPermissions()
        } else {
            getLastLocation()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        mFusedLocationClient!!.lastLocation
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful && task.result != null) {
                    mLastLocation = task.result
                    latitude = mLastLocation?.latitude.toString()
                    longitude = mLastLocation?.longitude.toString()

                    btnPermiteLocalization.visibility = View.GONE
                    rvPets.visibility = View.VISIBLE
                    progressBar.visibility = View.VISIBLE
                    getDataAPI(page)
                } else {
                    Log.w("TAG", "getLastLocation:exception", task.exception)
                }
            }
    }


    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(
            this@MainActivity,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
            REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

        if (shouldProvideRationale) {
            startLocationPermissionRequest()
        } else {
            startLocationPermissionRequest()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        Log.i("TAG", "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                Log.i("TAG", "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation()
            } else {

            }
        }
    }


    companion object {
        private const val REQUEST_PERMISSIONS_REQUEST_CODE = 100
    }
}
