package br.com.mobot.locapets.models

import br.com.mobot.locapets.models.list.PetModel
import java.io.Serializable

data class DetailPetsModel(var pet: PetModel) : Serializable