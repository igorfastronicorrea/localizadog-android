package br.com.mobot.locapets.models

import java.io.Serializable

data class MessageModel(var message: String) : Serializable