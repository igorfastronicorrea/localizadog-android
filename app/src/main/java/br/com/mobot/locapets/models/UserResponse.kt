package br.com.mobot.locapets.models

import java.io.Serializable

data class UserResponse(
    val _id: String,
    val name: String,
    val email: String,
    val tokenGoogle: String
) : Serializable