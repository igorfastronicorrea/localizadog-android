package br.com.mobot.locapets.ui.createPost

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.mobot.locapets.R
import br.com.mobot.locapets.adapter.register.ImageAdapter
import br.com.mobot.locapets.models.DataImage
import br.com.mobot.locapets.models.MessageModel
import br.com.mobot.locapets.models.PetCreateModel
import br.com.mobot.locapets.network.APIClient
import br.com.mobot.locapets.network.APIInterface
import br.com.mobot.locapets.utils.BrPhoneNumberFormatter
import br.com.mobot.locapets.utils.Constants
import br.com.mobot.locapets.utils.ManagePermissions
import br.com.mobot.locapets.utils.UserPreferences
import com.shivtechs.maplocationpicker.LocationPickerActivity
import com.shivtechs.maplocationpicker.MapUtility
import kotlinx.android.synthetic.main.act_register.*
import kotlinx.android.synthetic.main.act_register.myToolbar
import kotlinx.android.synthetic.main.activity_pet_detail.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.lang.ref.WeakReference


class RegisterActivity : AppCompatActivity() {

    private val ADDRESS_PICKER_REQUEST = 1020
    private val GALLERY_REQUEST_CODE = 1010
    private var dataPhoto: ArrayList<String> = ArrayList()
    var adapterImageUpload: ImageAdapter? = null
    var latitude = ""
    var longitude = ""
    var city = ""
    val list = listOf<String>(Manifest.permission.READ_EXTERNAL_STORAGE)
    private lateinit var managePermissions: ManagePermissions
    private val TAG = "PermissionDemo"
    private val RECORD_REQUEST_CODE = 101

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_register)

        MapUtility.apiKey = resources.getString(R.string.api_key)
        myToolbar.setNavigationOnClickListener({ view -> onBackPressed() })

        btnAddLocation.setOnClickListener {
            val i = Intent(this, LocationPickerActivity::class.java)
            startActivityForResult(i, ADDRESS_PICKER_REQUEST)
        }

        btnAddPhoto.setOnClickListener {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                makeRequest()
            } else {
                showGalleryImage()
            }

        }

        val addLineNumberFormatter = BrPhoneNumberFormatter(WeakReference<EditText>(edtWhatsApp))
        edtWhatsApp.addTextChangedListener(addLineNumberFormatter)

        adapterImageUpload = ImageAdapter(dataPhoto, this)
        rvImages.adapter = adapterImageUpload
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvImages.layoutManager = layoutManager

        btnCreatePost.setOnClickListener {

            if (!edtName.text.isEmpty() && !edtDescription.text.isEmpty() && !edtWhatsApp.text.isEmpty() && !latitude.isEmpty() && !latitude.equals("") ) {
                btnCreatePost.isEnabled = false
                btnCreatePost.isClickable = false
                sendPost(latitude, longitude, city)
            } else {
                showMessageWhiteFields("", "Por favor preencha todos os campos!")
            }
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ADDRESS_PICKER_REQUEST) {
                try {
                    var address = data?.getStringExtra(MapUtility.ADDRESS)
                    latitude = (data?.getDoubleExtra(MapUtility.LATITUDE, 0.0)).toString()
                    longitude = (data?.getDoubleExtra(MapUtility.LONGITUDE, 0.0)).toString()
                    city = (data?.getStringExtra(MapUtility.ADDRESS)).toString()
                    btnAddLocation.text = address.toString()
                } catch (e: Exception) {
                    Log.i("TAG", "error")
                }
            } else if (requestCode == GALLERY_REQUEST_CODE) {
                val selectedImage = data?.data
                uploadImage(selectedImage!!)
            }
        }

    }


    fun uploadImage(image: Uri) {

        var dialogImage = ProgressDialog(this)
        dialogImage!!.setMessage("Enviando imagem! Aguarde...")
        dialogImage!!.show()

        var file = File(getPath(image))
        var fileImage = RequestBody.create(MediaType.parse(contentResolver.getType(image)), file)
        var body = MultipartBody.Part.createFormData("file", "pets.png", fileImage)

        val retrofit =
            APIClient.getDataApi(Constants.BASE_URL_UPLOADIMAGE).create(APIInterface::class.java)
        val call = retrofit.uploadImage(body)

        call.enqueue(object : Callback<DataImage> {
            override fun onFailure(call: Call<DataImage>, t: Throwable) {
                if (dialogImage != null) {
                    dialogImage.dismiss()
                }
            }

            override fun onResponse(call: Call<DataImage>, response: Response<DataImage>) {
                if (response.body() != null) {
                    dataPhoto.add(response.body()!!.url)
                    adapterImageUpload!!.notifyDataSetChanged()

                }

                if (dialogImage != null) {
                    dialogImage.dismiss()
                }

            }

        })

    }

    fun getPath(uri: Uri): String {
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = managedQuery(uri, projection, null, null, null)
        val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        return cursor.getString(column_index)
    }

    fun sendPost(latitude: String, longitude: String, city: String) {

        var dialogPost = ProgressDialog(this)
        dialogPost!!.setMessage("Cadastrando! Aguarde...")
        dialogPost!!.show()


        var whatsapp = edtWhatsApp.text.toString()
        whatsapp = whatsapp.replace("(", "")
        whatsapp = whatsapp.replace(")", "")
        whatsapp = whatsapp.replace("-", "")
        whatsapp = whatsapp.replace(" ", "")


        val sharedPreferences = UserPreferences(this)

        var userId = sharedPreferences.getUserId()

        var bodyPet: PetCreateModel = PetCreateModel(
            edtName.text.toString(),
            edtDescription.text.toString(),
            dataPhoto,
            latitude,
            longitude,
            city,
            "",
            whatsapp
        )

        val retrofit =
            APIClient.getDataApi(Constants.BASE_URL_UPLOADIMAGE).create(APIInterface::class.java)
        val call = retrofit.createPost(userId, bodyPet)

        call.enqueue(object : Callback<MessageModel> {
            override fun onFailure(call: Call<MessageModel>, t: Throwable) {
                if (dialogPost != null) {
                    dialogPost.dismiss()
                    btnCreatePost.isEnabled = true
                    btnCreatePost.isClickable = true
                }
            }

            override fun onResponse(call: Call<MessageModel>, response: Response<MessageModel>) {
                if (response.body() != null) {
                    if (dialogPost != null) {
                        dialogPost.dismiss()
                    }
                    showMessage("Pet Cadastrado", "Essa publicação irá expirar em 7 dias!")
                }
            }

        })
    }

    fun showGalleryImage() {
        val i = Intent(Intent.ACTION_PICK)
        i.setType("image/*")
        val mimeTypes = arrayOf("image/jpeg", "image/png")
        i.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        startActivityForResult(i, GALLERY_REQUEST_CODE)
    }

    fun showMessage(title: String, message: String) {
        val builder = AlertDialog.Builder(this)

        builder.setTitle(title)
        builder.setMessage(message)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("Ok") { dialog, which ->
            finish()
        }

        builder.show()
    }

    fun showMessageWhiteFields(title: String, message: String) {
        val builder = AlertDialog.Builder(this)

        builder.setTitle(title)
        builder.setMessage(message)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("Ok") { dialog, which ->
        }

        builder.show()
    }


    private fun makeRequest() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
            RECORD_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            RECORD_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                    showGalleryImage()
                }
            }
        }
    }

}

// Extension function to show toast message
fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}