package br.com.mobot.locapets.ui.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import br.com.mobot.locapets.R
import br.com.mobot.locapets.models.DetailPetsModel
import br.com.mobot.locapets.network.APIClient
import br.com.mobot.locapets.network.APIInterface
import br.com.mobot.locapets.utils.Constants
import com.bumptech.glide.Glide
import com.daimajia.slider.library.Animations.DescriptionAnimation
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.stfalcon.imageviewer.StfalconImageViewer
import kotlinx.android.synthetic.main.activity_pet_detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DetailActivity : AppCompatActivity(), BaseSliderView.OnSliderClickListener,
    ViewPagerEx.OnPageChangeListener {

    var petDetail: DetailPetsModel? = null
    var petId = ""
    var lostAt = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pet_detail)


        val intent = getIntent();
        petId = intent.getStringExtra("idPet")
        lostAt = intent.getStringExtra("lostAt")

        myToolbar.setNavigationOnClickListener({ view -> onBackPressed() })

        getDetail()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun getDetail() {
        val retrofit =
            APIClient.getDataApi(Constants.BASE_URL_UPLOADIMAGE).create(APIInterface::class.java)
        val call = retrofit.getDetail(petId)

        call.enqueue(object : Callback<DetailPetsModel> {
            override fun onFailure(call: Call<DetailPetsModel>, t: Throwable) {
            }

            override fun onResponse(
                call: Call<DetailPetsModel>,
                response: Response<DetailPetsModel>
            ) {
                if (response.body() != null) {
                    petDetail = response.body()

                    populeSliderEFields()
                }
            }

        })
    }


    fun populeSliderEFields() {
        for (i in 0 until petDetail!!.pet.photo.size) {
            val textSliderView = TextSliderView(this)
            // initialize a SliderLayout
            textSliderView
                .image(petDetail!!.pet.photo[i])
                .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                .setOnSliderClickListener(this)

            slider.addSlider(textSliderView)
        }


        slider.setPresetTransformer(SliderLayout.Transformer.Default)
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
        slider.setCustomAnimation(DescriptionAnimation())
        slider.setDuration(1000000)
        slider.addOnPageChangeListener(this)


        txtName.text = petDetail!!.pet.name
        txtDescription.text = petDetail!!.pet.description
        txtTimeLost.text = lostAt
        txtLocationLost.text = "Última vez visto há ${petDetail!!.pet.km} km de você"

        if (petDetail!!.pet.whatsapp != null) {
            if (!petDetail!!.pet.whatsapp.isEmpty() || !petDetail!!.pet.whatsapp.equals("")) {
                btnWhatsapp.visibility = View.VISIBLE
                btnWhatsapp.setOnClickListener { openWhats() }
            } else {
                btnWhatsapp.visibility = View.GONE
            }
        } else {
            btnWhatsapp.visibility = View.GONE
        }

    }

    fun openWhats() {
        val url = "https://api.whatsapp.com/send?phone=+55${petDetail!!.pet.whatsapp}"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }

    override fun onSliderClick(slider: BaseSliderView?) {

        StfalconImageViewer.Builder<String>(this, petDetail!!.pet.photo) { view, image ->
            Glide.with(this).load(image).into(view)
        }
            .withStartPosition(0)
            .show(true)
    }

    override fun onPageScrollStateChanged(state: Int) {
        //   TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPageSelected(position: Int) {

    }

}